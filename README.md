Alex Szilagyi
==================
This repo contains solutions to Project Euler problems in Java.

Euler problems can be found at https://projecteuler.net/