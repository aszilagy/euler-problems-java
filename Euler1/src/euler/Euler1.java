/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler;

/**
 *
 * @author Alex Problem from https://projecteuler.net/problem=1
 */
public class Euler1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        long startTime = System.nanoTime();
        System.out.println("Result : " + calcProduct());

        long endTime = System.nanoTime();
        long totalTime = (endTime - startTime) / 1000000;
        System.out.println("Total Time of Program: " + totalTime);
    }

    private static int calcProduct() {
        int product = 0;
        int max = 1000;

        for (int i = 0; i < max; i++) {
            if (((i % 3) == 0) || ((i % 5) == 0)) {
                product += i;
            }
        }
        return product;
    }
}
