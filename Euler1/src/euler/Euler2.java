/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler;

/**
 *
 * @author Alex Szilagyi Problem from https://projecteuler.net/problem=2
 */
public class Euler2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        long startTime = System.nanoTime();
        System.out.println("Result: " + sumFibb());
        long endTime = System.nanoTime();
        long totalTime = (endTime - startTime) / 1000000;
        System.out.println("Total Time of Program: " + totalTime);
    }

    public static long sumFibb() {
        long max = 4000000;
        long finalResult = 0;
        long fib1 = 1;
        long fib2 = 1;
        long result = 0;

        while (result < max) {
            result = fib1 + fib2;
            fib1 = fib2;
            fib2 = result;

            if ((result % 2) == 0) {
                finalResult += result;
            }
        }
        return finalResult;
    }

}
